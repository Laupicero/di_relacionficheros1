﻿using System;
using System.IO;

namespace RelacionFicheros1
{
    class Program
    {
        static void Main(string[] args)
        {
            Boolean menuPrincipalActivado = true;

            Console.WriteLine("-----------------------------------------------------------------------");
            Console.WriteLine("\t\t REALIZACIÓN EJERCICIOS DE FICHEROS");
            Console.WriteLine("\n\t\t PROFESOR: ÁLVARO LÓPEZ JURADO");
            Console.WriteLine("\t\t ALUMNO: LAURA LUCENA BUENDIA\n");
            Console.WriteLine("-----------------------------------------------------------------------");
            Console.WriteLine("\t\t PULSE CUALQUIER LETRA PARA CONTINUAR");
            Console.WriteLine("-----------------------------------------------------------------------");
            pulsarParaSeguir();
            
            
            //MENÚ PRINCIPAL DE LA APP
            try
            {
                do
                {
                    Console.WriteLine("-----------------------------------------------");
                    Console.WriteLine("\t\t MENÚ PRINCIPAL");
                    Console.WriteLine("-----------------------------------------------");
                    Console.WriteLine("[1]- Volcar contenido de un fichero a otro");
                    Console.WriteLine("[2]- Buscar frase");
                    Console.WriteLine("[3]- Duplicar/Copiar Fichero");
                    Console.WriteLine("[4]- Saber nota media de matemáticas y la del mejor alumno de informática.");
                    Console.WriteLine("[5]- Combinación 2 Ficheros. Mezcla de líneas");
                    Console.WriteLine("[6]- Suma del fichero 'enteros.txt'");
                    Console.WriteLine("[7]- SALIR");
                    Console.WriteLine("--------------------------------------\n");

                    Console.Write("SELECCIONE QUE DESEA HACER:");
                    int opcion = Convert.ToInt32(Console.ReadLine());


                    //Llamará a un método, en la mayoría de casos, que realizará el ejercicio que deseemos
                    switch (opcion)
                    {
                        case 1:
                            Console.Clear();
                            volcarContenidoDeUnFicheroAOtro();
                            Console.WriteLine("\n-----------------------------------------------------------------------");
                            Console.WriteLine("\t\t PULSE CUALQUIER LETRA PARA CONTINUAR");
                            Console.WriteLine("-----------------------------------------------------------------------");
                            pulsarParaSeguir();
                            break;

                        case 2:
                            Console.Clear();
                            buscarFraseEnLineasDeUnFichero();
                            Console.WriteLine("\n-----------------------------------------------------------------------");
                            Console.WriteLine("\t\t PULSE CUALQUIER LETRA PARA CONTINUAR");
                            Console.WriteLine("-----------------------------------------------------------------------");
                            pulsarParaSeguir();
                            break;

                        case 3:
                            Console.Clear();
                            duplicarUnFichero();
                            Console.WriteLine("\n-----------------------------------------------------------------------");
                            Console.WriteLine("\t\t PULSE CUALQUIER LETRA PARA CONTINUAR");
                            Console.WriteLine("-----------------------------------------------------------------------");
                            pulsarParaSeguir();
                            break;

                        case 4:
                            Console.Clear();
                            notaMediaYMejorNota();
                            Console.WriteLine("\n-----------------------------------------------------------------------");
                            Console.WriteLine("\t\t PULSE CUALQUIER LETRA PARA CONTINUAR");
                            Console.WriteLine("-----------------------------------------------------------------------");
                            pulsarParaSeguir();
                            break;

                        case 5:
                            Console.Clear();
                            crearFicheroUnion();
                            Console.WriteLine("\n-----------------------------------------------------------------------");
                            Console.WriteLine("\t\t PULSE CUALQUIER LETRA PARA CONTINUAR");
                            Console.WriteLine("-----------------------------------------------------------------------");
                            pulsarParaSeguir();
                            break;

                        case 6:
                            Console.Clear();
                            sumaEnteros();
                            Console.WriteLine("\n-----------------------------------------------------------------------");
                            Console.WriteLine("\t\t PULSE CUALQUIER LETRA PARA CONTINUAR");
                            Console.WriteLine("-----------------------------------------------------------------------");
                            pulsarParaSeguir();
                            break;

                        case 7:
                            menuPrincipalActivado = false;
                            Console.Clear();
                            Console.WriteLine("\n---------------------------------------------------");
                            Console.WriteLine("\t\tADIOS");
                            Console.WriteLine("\n\t Esperamos verte pronto");
                            Console.WriteLine("\n---------------------------------------------------");
                            break;

                        default:
                            break;
                    }

                    } while (menuPrincipalActivado);

            }
            catch (FormatException e)
            {
                Console.WriteLine("Inserte un carácter numérico" + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error, desconocido." + e.Message);

            }//Fin Try-Catch Menu Principal

            Console.ReadLine();

        }//Fin del método main

       

        /// -------------------------------------------
        /// -------------------------------------------
        ///         MÉTODOS PRINCIPALES
        /// -------------------------------------------
        /// -------------------------------------------



        /// <summary>
        /// -----------------------------
        ///     MÉTODO DEL EJERCICIO 1
        /// -----------------------------
        /// Vuelca todo el contenido de un fichero de texto a otro. 
        /// Los nombres de ambos ficheros se indican como parámetros en la línea de comandos.
        /// </summary>
        private static void volcarContenidoDeUnFicheroAOtro()
        {
            StreamReader ficheroLeer;
            StreamWriter ficheroVolcar;
            String nombreFicheroVolcado, nombreFicheroAVolcar, linea;

            Console.Write("Indique el nombre del fichero que quiere seleccionar: ");
            nombreFicheroVolcado = Console.ReadLine() + ".txt";

            //Sino existe el fichero lo creamos
            if (!File.Exists(nombreFicheroVolcado))           
                opcionCrearFichero(nombreFicheroVolcado);
            
            //Ahora sellecionamos donde queremos volcar los datos
            Console.Write("Ahora seleccione el nombre donde quiere volcar los datos del fichero '" + nombreFicheroVolcado + "'" +
                "\n(No añada '.txt', sólo el nombre del fichero de texto): ");
            nombreFicheroAVolcar = Console.ReadLine() + ".txt";

            //Comprobamos si existe, sino lo creamos
            if (File.Exists(nombreFicheroAVolcar))
            {
                ficheroVolcar = File.AppendText(nombreFicheroAVolcar);
            }
            else
            {
                ficheroVolcar = File.CreateText(nombreFicheroAVolcar);
            }

            //Leemos el contenido del fichero 'nombreFicheroVolcado'
            ficheroLeer = File.OpenText(nombreFicheroVolcado);
            linea = ficheroLeer.ReadLine();
            //Seguimos leyendo hasta que no encuentre ninguna línea
            while (linea != null)
            {
                ficheroVolcar.WriteLine(linea);
                linea = ficheroLeer.ReadLine();
            }

            //Cerramos los flujos
            ficheroVolcar.Close();
            ficheroLeer.Close();
        }

        



        /// <summary>
        /// -----------------------------
        ///     MÉTODO DEL EJERCICIO 2
        /// -----------------------------
        ///  método que pide al usuario el nombre de un fichero de texto y una frase a buscar, 
        ///  y que muestra en pantalla todas las frases de ese fichero que contengan esa palabra/frase.
        ///  Cada frase precede del numero de linea.
        /// </summary>
        private static void buscarFraseEnLineasDeUnFichero()
        {
            Console.Write("¿Qué Frase/Palabra desea buscar? ");
            String frase = Console.ReadLine().ToLower();

            Console.Write("¿En qué fichero?: ");
            String ficheroBuscaFrase = Console.ReadLine() + ".txt";

            if (!File.Exists(ficheroBuscaFrase))
            {
                opcionCrearFichero(ficheroBuscaFrase);
            }
            //Leemos
                try
                {
                    StreamReader fichero = File.OpenText(ficheroBuscaFrase);

                    //Leemos el Contenido
                    String lineaFichero = fichero.ReadLine();
                    int contadorLineas = 1;
                    Boolean fraseEncontrada = false;

                    while (lineaFichero != null)
                    {
                        lineaFichero = lineaFichero.ToLower();
                        if (lineaFichero.Contains(frase))
                        {
                            Console.WriteLine(contadorLineas + "-Línea: " + lineaFichero);
                            fraseEncontrada = true;
                        }
                        lineaFichero = fichero.ReadLine();
                        contadorLineas += 1;
                    }

                    if (!fraseEncontrada) { Console.WriteLine("No se encontraron resultados\n"); }
                    fichero.Close();
                }
                catch (Exception) { Console.WriteLine("ERROR"); }
        }



        /// <summary>
        /// -----------------------------
        ///     MÉTODO DEL EJERCICIO 3
        /// -----------------------------
        ///  Un programa que duplica un fichero, copiando todo su contenido a un nuevo fichero.
        ///  El nombre de ambos ficheros se lee de la línea de comandos.
        /// </summary>
        private static void duplicarUnFichero()
        {
            Console.Write("¿Qué fichero desea copiar (No inserte '.txt')? ");
            String nombreFicheroACopiar = Console.ReadLine() + ".txt";

            Console.Write("¿Cómo se llama el fichero donde va a plasmar los datos (No inserte '.txt')? ");
            String nuevoFicheroCopiado = Console.ReadLine() + ".txt";

            if (!File.Exists(nombreFicheroACopiar))
                opcionCrearFichero(nombreFicheroACopiar);

            File.Copy(nombreFicheroACopiar, nuevoFicheroCopiado);
        }


        /// <summary>
        /// -----------------------------
        ///     MÉTODO DEL EJERCICIO 4
        /// -----------------------------
        ///  Se  tiene un fichero tipo texto conteniendo en cada línea el nombre y las calificaciones en matemáticas e informática de uno de los alumnos de un grupo.
        ///  A partir de dichos datos escriba en otro fichero la nota media del grupo en matemáticas 
        ///  y la mejor nota en informática así como el nombre del alumno que la obtuvo.
        /// </summary>
        private static void notaMediaYMejorNota()
        {

            if (File.Exists("notas.txt"))
            {
                //Var ayuda
                StreamReader ficheroNotas = File.OpenText("notas.txt");
                char[] separadores = { ' ' };
                int contadorAlum = 1;

                //Var para guardar datos
                String nombreAlumnoNota = "";
                float mediaGrupalMates = 0;
                float notaMasAltaInformatica = 0;

                //Aqui almacenaremos la linea
                String lineaAlumnoNotas = ficheroNotas.ReadLine();

                while (lineaAlumnoNotas != null)
                {
                    //Almacenaremos los datos de la linea en un array (por cada linea se ira sobreescribiendo)
                    String[] datosAlumno = lineaAlumnoNotas.Split(separadores);

                    //Recorremos los datos de ese alumno en cuestion
                    for (int i = 0; i < datosAlumno.Length; i++)
                    {
                        switch (i)
                        {
                            case 1:
                                if (notaMasAltaInformatica < float.Parse(datosAlumno[i])) 
                                {
                                    notaMasAltaInformatica = float.Parse(datosAlumno[i]);
                                    nombreAlumnoNota = datosAlumno[i - 1].Trim();
                                }
                                break;

                            case 2:
                                mediaGrupalMates += float.Parse(datosAlumno[i]);
                                break;
                        }
                    }
                    contadorAlum += 1;
                    lineaAlumnoNotas = ficheroNotas.ReadLine();
                }

                //Mostramos los datos por consola
                float media = mediaGrupalMates / contadorAlum;
                Console.WriteLine("La media grupal de mates es: " + media);
                Console.WriteLine("El alumno con más nota en informática es '" + nombreAlumnoNota + "', que tiene '" + notaMasAltaInformatica + "' de nota");

                //Nos creamos un fichero con los resultados
                StreamWriter ficheroResultado = File.CreateText("resultadoNotas.txt");
                ficheroResultado.WriteLine("La media grupal de mates es: " + media);
                ficheroResultado.WriteLine("El alumno con más nota en informática es '" + nombreAlumnoNota + "', que tiene '" + notaMasAltaInformatica + "'");

                //Cerramos
                ficheroNotas.Close();
                ficheroResultado.Close();
            }
            else { Console.WriteLine("\n¡Archivo no encontrado. Debería crearlo primero!"); }
        }


        /// <summary>
        /// -----------------------------
        ///     MÉTODO DEL EJERCICIO 5
        /// -----------------------------
        /// Permite la combinación de dos ficheros de texto, cuyo nombre se pasa en la línea de comandos.
        /// Este método creará un tercer fichero llamado 'union.txt' en el que iremos mezclando las líneas de los ficheros anteriores. 
        /// 
        /// Es decir se escribirá una línea del fichero1 y a continuación una línea del fichero2. 
        /// Si alguno de los ficheros tiene mayor contenido, se escribirán todas las líneas que queden seguidas.
        /// </summary>
        private static void crearFicheroUnion()
        {
            String nombreFich1, nombreFich2;

            // Creamos ambos ficheros de texto
            //Primer fichero
            Console.Write("Inserte el nombre del primer fichero (Sin '.txt'): ");
            nombreFich1 = Console.ReadLine() + ".txt";

            if (!File.Exists(nombreFich1))
                opcionCrearFichero(nombreFich1);

            //Segundo fichero
            Console.Write("Inserte el nombre del segundo fichero (Sin '.txt'): ");
            nombreFich2 = Console.ReadLine() + ".txt";

            if (!File.Exists(nombreFich2))
                opcionCrearFichero(nombreFich2);


            // Creamos el fichero union y volcamos el contenido de ambos
            StreamWriter ficheroEsc = File.CreateText("union.txt");

            //Leemos en ambos
            try
            {
                StreamReader fichero1 = File.OpenText(nombreFich1);
                StreamReader fichero2 = File.OpenText(nombreFich2);
                String lineaFichero1 = fichero1.ReadLine();
                String lineaFichero2 = fichero2.ReadLine();
                int contadorLineas = 1;

                do
                {
                    while (lineaFichero1 != null || lineaFichero2 != null)
                    {
                        if (lineaFichero1 != null)
                        {
                            if (contadorLineas % 2 != 0)
                            {
                                ficheroEsc.WriteLine(lineaFichero1);
                                lineaFichero1 = fichero1.ReadLine();
                            }
                        }

                        if (lineaFichero2 != null)
                        {
                            if (contadorLineas % 2 == 0)
                            {
                                ficheroEsc.WriteLine(lineaFichero2);
                                lineaFichero2 = fichero2.ReadLine();
                            }
                        }

                        contadorLineas++;
                    }
                    fichero1.Close();
                    fichero2.Close();
                    ficheroEsc.Close();

                } while (lineaFichero1 != null && lineaFichero2 != null);
            }
            catch (Exception e) { Console.WriteLine("ERROR: " + e.Message); }
        }


        /// <summary>
        /// -----------------------------
        ///     MÉTODO DEL EJERCICIO 6
        /// -----------------------------
        /// Disponemos de un fichero de texto llamado enteros.txt que contiene los siguientes números enteros separados por espacios en blanco o comas: 
        /// 34,45,23 8, 9
        /// 12 23
        /// Este método lee el contenido del fichero y nos muestra los números, la cantidad de números leídos y su suma.
        /// </summary>
        private static void sumaEnteros()
        {
            if (!File.Exists("enteros.txt"))
            {
                Console.WriteLine("Debe crear primero el fichero de texto 'enteros.txt'" +
                    "\nCon números enteros separados por espacios en blanco o comas. EJ: (34, 45, 23 8, 9)");

                opcionCrearFichero("enteros.txt");
            }
                try
                {
                    int contadorNumeros = 0;
                    int sumaNumeros = 0;
                    char[] separators = { ',', ' ' };
                    StreamReader fichero = File.OpenText("enteros.txt");
                    String lineaFichero = fichero.ReadLine();

                    Console.WriteLine("Números: ");

                    //Leemos
                    while (lineaFichero != null)
                    {
                        //Obtenemos todos los elementos
                        String[] elementos = lineaFichero.Split(separators);

                        //Mostramos todos los numeros
                        mostrarSoloNumerosYSumamos(ref contadorNumeros, ref sumaNumeros, elementos);
                        lineaFichero = lineaFichero.ToLower();

                        lineaFichero = fichero.ReadLine();
                    }
                    Console.WriteLine("\nHay '" + contadorNumeros + "' números: ");
                    Console.WriteLine("Suma de todos los números: " + sumaNumeros);

                }
                catch (Exception) { Console.WriteLine("ERROR"); }
            
                
        }



        /// -------------------------------------------
        /// -------------------------------------------
        ///         MÉTODOS AUXILIARES
        /// -------------------------------------------
        /// -------------------------------------------
        /// 

        /// <summary>
        /// Nos da la opcion de crear o no un fichero cuando se presenta la ocasión
        /// </summary>
        private static void opcionCrearFichero(String nombreFichero)
        {
            Boolean opcionCorrectaCrearFichero = true;
            do
            {
                Console.WriteLine("Fichero no existente, seleccione otro, por favor.\n" +
                "¿Desearía crearlo ahora?" +
                "\n[0]-NO" +
                "\n[1]-SI");

                try
                {
                    int opcionCrearFichero = Convert.ToInt32(Console.ReadLine());

                    if (opcionCrearFichero == 1)
                        crearFichero(nombreFichero);

                }
                catch (FormatException e)
                {
                    opcionCorrectaCrearFichero = false;
                    Console.WriteLine("Inserte un carácter numérico" + e.Message);
                }
                catch (Exception e)
                {
                    opcionCorrectaCrearFichero = false;
                    Console.WriteLine("Error, desconocido." + e.Message);

                }
            } while (!opcionCorrectaCrearFichero);
        }

        // Nos creará un fichero línea por línea
        // que irá introduciendo el usuario hasta escribir la palabra 'fin'
        private static void crearFichero(String nombreFichero)
        {
            StreamWriter ficheroEsc = File.CreateText(nombreFichero);
            String frase = "";
            int contadorLineas = 1;


            Console.WriteLine("El fichero '" + nombreFichero + "' ha sido creado" +
                "\n(Para saltar de línea pulsa 'enter'. Para dejar de insertar líneas en el fichero escribe 'fin')" +
                "\n\n PULSE CUALQUIER TECLA PARA EMPEZAR A ESCRIBIR");

            pulsarParaSeguir();

            while (frase.ToLower() != "fin")
            {
                Console.Write("Inserte el contenido de la línea '" + contadorLineas + "': ");
                frase = Console.ReadLine();

                if(frase.ToLower() != "fin")
                {
                    ficheroEsc.WriteLine(frase);
                    contadorLineas++;
                }                                   
            }
            ficheroEsc.Close();
        }



        /// <summary>
        /// Método de ayuda del ejercicio 6
        /// 
        /// Nos muestra sólo los caracteres numéricos y nos suma estos
        /// </summary>
        /// <param name="contadorNumeros"></param>
        /// <param name="elementos"></param>
        private static void mostrarSoloNumerosYSumamos(ref int contadorNumeros, ref int sumaNumeros, String[] elementos)
        {
            foreach (String elem in elementos)
            {
                if(elem.Length != 0 && Convert.ToInt32(elem.Trim()) != null)
                {
                    Console.Write(elem + "\t");
                    contadorNumeros++;
                    sumaNumeros += Convert.ToInt32(elem.Trim());
                }
            }
        }


        // Nos borra todo el contenido de la consola al detectar que hemos pulsado una tecla
        private static void pulsarParaSeguir()
        {
            Console.ReadKey();
            Console.Clear();
        }
    }
}
